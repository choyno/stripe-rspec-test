# app/models/concerns/stripe_transaction.rb
module StripeTransaction
  DATA_RETRIEVE_LIMIT = 50
  TRANSFER_GROUP_FORMAT = 'TXN_'

  include StripeHelper
  include Chargeable
  #include Payable
  #include Refundable
  #include MakeableCard
  #include InquirableBalance
  include Transferrable
end
