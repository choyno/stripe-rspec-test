# app/models/concerns/stripe_transaction/chargeable.rb
module StripeTransaction
  # This module is use to charge customer
  module Chargeable
    DATA_RETRIEVE_LIMIT = StripeTransaction::DATA_RETRIEVE_LIMIT
    TRANSFER_GROUP_FORMAT = StripeTransaction::TRANSFER_GROUP_FORMAT

    def first_or_create_charges(txn)
      # Now, guides will be paid with transfer_group
      transfer_group = "#{TRANSFER_GROUP_FORMAT}#{txn.id}"
      details = compose_details_no_destination(txn, transfer_group)
      return if details[:amount] <= 0
      first_or_create_charge(txn, details)
    end

    def compose_details_no_destination(txn, transfer_group = nil)
      currency = PaymentTransaction::DEFAULT_CURRENCY
      details = {
        amount: txn.total_amount, currency: currency,
        customer: txn.user.stripe_customer_id
      }
      details[:description] = I18n.t(
        '.transaction.charge_guest_description', email: txn.user.email
      )
      details[:transfer_group] = transfer_group if transfer_group.present?
      total_amount = txn.amount.to_f
      total_refund = txn.compute_refund(nil, total_amount)
      amount = total_amount - total_refund
      # in case, total refund is greater than total amount
      # [with coupons and refund]
      amount = total_amount if amount < 0
      details[:amount] = stripe_amount(amount, currency)
      details[:metadata] = prepare_metadata(txn, transfer_group)
      details
    end

    def prepare_metadata(txn, transfer_group = nil)
      meta = { charge_only: 'yes' }
      meta[:with_transfer_group] = 'yes' if transfer_group.present?
      txn.reservations.each do |id, _reserv_info|
        meta["main_guide_fee_#{id}"] = txn.main_guide_payment(id)
        meta["support_guide_fee_#{id}"] = txn.support_guide_payment(id)
        meta["extra_fee_#{id}"] = txn.others_price(id)
      end
      meta
    end

    def compose_details(txn, guide, reserv_id)
      currency = PaymentTransaction::DEFAULT_CURRENCY
      details = {
        amount: txn.total_amount, currency: currency,
        customer: txn.user.stripe_customer_id
      }
      guide_type = txn.booking.guide.id == guide.id ? 'main' : 'support'
      opts = { guide_type: guide_type, purpose: 'guide_payment' }
      destination_details = { account: guide.stripe_account_id, amount: 0 }
      guide_amount = txn.send("#{guide_type}_guide_price", reserv_id)
      # other expenses should not go to Huber but to Main Guide stripe account
      guide_amount += txn.others_price if guide_type == 'main'
      refund_amount = txn.compute_refund(reserv_id, guide_amount, opts)
      guide_payment = guide_amount - refund_amount -
                      txn.compute_guide_charge_fee(reserv_id)
      destination_details[:amount] = stripe_amount(guide_payment, currency)
      details[:destination] = destination_details
      details[:description] = I18n.t(
        '.transaction.charge_description',
        user_type: guide_type.capitalize, email: guide.email
      )
      reserv_amount =
        if guide_type == 'support'
          guide_amount
        else
          txn.amount.to_f - txn.support_price_excluding_refunded
        end
      reserv_refund = txn.compute_refund(
        reserv_id, reserv_amount, opts.except(:purpose)
      )
      details[:amount] = stripe_amount(reserv_amount - reserv_refund, currency)
      details[:metadata] = {
        user_type: guide_type, guide_id: guide.id, reservation_id: reserv_id,
        guide_payment: guide_payment.to_f
      }
      details
    end

    def first_or_create_charge(txn, details)
      retrieve_charge_only(txn, details) || Stripe::Charge.create(details)
    end

    def retrieve_charge_only(txn, details)
      Stripe::Charge.list(limit: DATA_RETRIEVE_LIMIT).data.select do |charge|
        meta = charge.metadata
        user_type = details[:metadata][:user_type]
        condition =
          if user_type == 'main' || user_type.blank?
            meta[:reservation_ids] == txn.reservations.keys.join(',')
          else
            meta[:reservation_id] == details[:metadata][:reservation_id].to_s
          end
        condition &&= meta[:charge_only] == 'yes' unless user_type
        condition && !charge.refunded && meta[:transaction_id].to_i == txn.id &&
          meta[:booking_id].to_i == txn.booking_id &&
          meta[:user_id].to_i == txn.user_id
      end.first
    end

    def retrieve_charge(txn, details)
      Stripe::Charge.list(limit: DATA_RETRIEVE_LIMIT).data.select do |charge|
        meta = charge.metadata
        if details[:metadata][:user_type] == 'main'
          meta[:reservation_ids] == txn.reservations.keys.join(',')
        else
          meta[:reservation_id] == details[:metadata][:reservation_id].to_s
        end &&
          !charge.refunded && meta[:transaction_id].to_i == txn.id &&
          meta[:booking_id].to_i == txn.booking_id &&
          meta[:user_id].to_i == txn.user_id
      end.first
    end
  end
end
