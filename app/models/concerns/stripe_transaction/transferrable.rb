# app/models/concerns/stripe_transaction/transferrable.rb
module StripeTransaction
  # This module is use to transfer from platform account to connect account
  module Transferrable
    DATA_RETRIEVE_LIMIT = 1
    TRANSFER_GROUP_FORMAT = StripeTransaction::TRANSFER_GROUP_FORMAT

    def make_transfer_payments!(validations, txn: self)
      transfers = {}
      txn.reservations.each do |reserv_id, _reserv|
        next unless txn.do_transfer_payments?(reserv_id)
        transfers["#{reserv_id}"] = txn.first_or_create_transfers!(reserv_id)
      end
      # per reservation & per guide validations
      validations = txn.validation_errors!(validations)
      txn.save_transfer_payments!(transfers) if transfers.present?
      validations
    end

    def first_or_create_transfers!(reserv_id, txn: self)
      transfers = []
      txn.guides_reservations(reserv_id).each do |guide|
        details = txn.compose_transfer_details(guide, reserv_id)
        next unless details[:amount] > 0 && txn.enough_balance?(details) &&
                    txn.no_transfer_payment_to_guide?(reserv_id, guide) &&
                    txn.guide_registered_in_stripe?(reserv_id, guide) &&
                    (transfer = txn.first_or_create_transfer(details)).present?
        transfers << transfer
        success_msg = I18n.t(
          '.transaction.auto_transfer_payment_success',
          txn_id: txn.id, reserve_id: reserv_id,
          email: "#{guide.email} (GuideID: #{guide.id})"
        )
        puts "\r\n #{success_msg}"
      end
      transfers
    end

    def compose_transfer_details(guide, reserv_id, txn: self)
      currency = PaymentTransaction::DEFAULT_CURRENCY
      details = {
        destination: guide.stripe_account_id,
        transfer_group: "#{TRANSFER_GROUP_FORMAT}#{txn.id}",
        amount: 0, currency: currency, metadata: {
          transaction_id: txn.id,
          guest_id: txn.user_id, guide_id: guide.id,
          booking_id: txn.booking_id, reservation_id: reserv_id
        }
      }
      guide_type = txn.booking.guide.id == guide.id ? 'main' : 'support'
      details[:amount] = stripe_amount(guide_fee(reserv_id, guide), currency)
      details[:description] = I18n.t(
        '.transaction.transfer_description',
        user_type: guide_type.capitalize, email: guide.email
      )
      details
    end

    def enough_balance?(details, txn: self)
      enough_balance = (balance = txn.retrieve_balance) &&
                       details[:amount] <= balance.amount.to_f
      unless enough_balance
        err_code = 'not_enough_balance'
        err_msg = I18n.t(
          ".transaction.#{err_code}",
          balance: balance.amount, amount: details[:amount],
          booking_id: details[:metadata][:booking_id],
          reservation_id: details[:metadata][:reservation_id]
        )
        txn.errors.add(:base, err_msg)
      end
      enough_balance
    end

    def guide_registered_in_stripe?(reserv_id, guide, txn: self)
      unless (registered_in_stripe = guide.can_receive_stripe_payments?)
        err_code = 'guide_no_stripe_account'
        err_msg = I18n.t(
          ".transaction.#{err_code}",
          booking_id: txn.booking_id, reservation_id: reserv_id,
          email: "#{guide.email} (GuideID: #{guide.id})"
        )
        txn.errors.add(:base, err_msg)
      end
      registered_in_stripe
    end

    def no_transfer_payment_to_guide?(reserv_id, guide, txn: self)
      reserv_transfers = txn.transfer_payments["#{reserv_id}"]
      no_transfer = reserv_transfers.try(:[], "#{guide.id}").blank?
      unless no_transfer
        err_code = 'transfer_to_guide_exists'
        err_msg = I18n.t(
          ".transaction.#{err_code}",
          booking_id: txn.booking_id, reservation_id: reserv_id,
          email: "#{guide.email} (GuideID: #{guide.id})"
        )
        txn.errors.add(:base, err_msg)
      end
      no_transfer
    end

    def first_or_create_transfer(details, txn: self)
      txn.retrieve_transfer(details) || Stripe::Transfer.create(details)
    end

    def retrieve_transfer(details, txn: self)
      Stripe::Transfer.list(
        limit: DATA_RETRIEVE_LIMIT,
        destination: details[:destination],
        transfer_group: details[:transfer_group]
      ).data.select do |transfer|
        meta = transfer.metadata
        meta[:guest_id].to_i == txn.user_id &&
          meta[:transaction_id].to_i == txn.id &&
          meta[:booking_id].to_i == txn.booking_id &&
          meta[:guide_id] == details[:metadata][:guide_id].to_s &&
          meta[:reservation_id] == details[:metadata][:reservation_id].to_s
      end.first
    end

    def do_transfer_payments?(reserv_id = nil, txn: self)
      err_msg, err_code = nil, nil
      if !(condition = txn.booking_offered?)
        # existing validation, reuse here
      elsif !(condition &&= txn.paid_already?)
        err_code = 'not_yet_paid'
        err_msg = I18n.t(".transaction.#{err_code}", booking_id: booking_id)
      elsif !(condition &&= txn.for_stripe_transfer?)
        err_code = 'not_for_stripe_transfer'
        err_msg = I18n.t(".transaction.#{err_code}", booking_id: booking_id)
      elsif reserv_id.present? &&
            txn.booking.reservations.find_by_id(reserv_id.to_i) &&
            !(condition &&= txn.to_charge_guest_reservations?(reserv_id))
        err_code = 'unchargeable_reservation'
        err_msg = I18n.t(
          ".transaction.#{err_code}",
          txn_type: 'Transfer', booking_id: txn.booking_id,
          reservation_id: reserv_id
        )
      elsif reserv_id.present? &&
            txn.cancelled_but_with_pay?(reserv_id) &&
            !(condition &&= txn.within_90_days_from_paid_date?)
        err_code = 'cancel_more_than_90_days'
        err_msg = I18n.t(
          ".transaction.#{err_code}",
          booking_id: booking_id, reservation_id: reserv_id
        )
      elsif reserv_id.present? &&
            !(condition &&= txn.scheduled_to_transfer_payment?(reserv_id))
        err_code = 'not_scheduled_to_transfer'
        err_msg = I18n.t(
          ".transaction.#{err_code}",
          booking_id: booking_id, reservation_id: reserv_id
        )
      end
      txn.errors.add(:base, err_msg) if err_msg.present? && err_code.present?
      condition
    end

    def for_stripe_transfer?(txn: self)
      txn.paid_already? &&
        (transfer_group = txn.payment_charges.values.first.transfer_group) &&
        transfer_group.starts_with?(TRANSFER_GROUP_FORMAT)
    end

    def no_transfer_payments?(reserv_id, txn: self)
      txn.transfer_payments["#{reserv_id}"].blank?
    end

    def scheduled_to_transfer_payment?(reserv_id)
      reservation = Reservation.find(reserv_id.to_i)
      tour_date = reservation.try(:schedule).try(:to_date)
      tour_date.present? && Time.zone.now.to_date > tour_date
    end

    def cancelled_but_with_pay?(reserv_id)
      reservation = Reservation.find_by(id: reserv_id.to_i)
      reservation.present? && reservation.cancelled_tour_with_pay?
    end

    def save_transfer_payments!(transfers, txn: self)
      txn.other_details['transfers'] ||= {}
      transfers.each do |reserv_id, my_transfers|
        txn.other_details['transfers']["#{reserv_id}"] ||= {}
        my_transfers.each do |my_transfer|
          txn.other_details['transfers']["#{reserv_id}"][
            "#{my_transfer.metadata[:guide_id]}"
          ] = my_transfer
        end
      end
      txn.save
    end

    def stop_transfer_info
      other_details["stop_transfer"] || "off"
    end

    def update_stop_transfer( status )
      other_details["stop_transfer"] = status
      save
    end

  end
end
