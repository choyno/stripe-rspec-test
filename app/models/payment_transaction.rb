# == Schema Information
#
# Table name: payment_transactions
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  booking_id       :integer
#  amount           :decimal(8, 2)    default(0.0), not null
#  paid             :boolean          default(FALSE)
#  charged_fee      :decimal(8, 2)    default(0.0)
#  stripe_charge_id :string
#  refund_details   :jsonb
#  other_details    :jsonb
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_payment_transactions_on_booking_id  (booking_id)
#  index_payment_transactions_on_user_id     (user_id)
#
class PaymentTransaction < ActiveRecord::Base
  include StripeTransaction

  after_find :set_charged_fee, unless: :done?
  after_find :set_currency, unless: :done?
  after_initialize :set_details
  after_find :set_reservation_details, unless: :done?
  after_find :set_amount, unless: :done?

  belongs_to :user
  belongs_to :booking

  DEFAULT_CURRENCY = 'jpy'
  PERCENT_CHARGE_FEE = 0.15
  GUIDE_CHARGE_FEE = 1500
  PERCENT_CONVERSION_FEE = 0.02

  validates :amount, presence: true

  scope :stop_transfer_on, (lambda do  search_stop_transfer("on") end)

  scope :scheduled_to_charge_guests, (lambda do
    where(stripe_charge_id: nil, paid: false)
    .joins(booking: :reservations).where('reservations.schedule IS NOT NULL')
  end)
  scope :paid_out, (lambda do
    where(
      "(payment_transactions.other_details->'payment_to_guides') IS NOT NULL"
    )
  end)
  scope :already_paid, (lambda do
    where_sql = "(payment_transactions.other_details->'charges') IS NOT NULL"
    where.not(stripe_charge_id: nil, paid: false).where(where_sql)
  end)
  scope :scheduled_transfer_payments, (lambda do
    where_sql = "CURRENT_DATE > COALESCE(reservations.schedule, NOW())::DATE
    AND COALESCE(payment_transactions.other_details->>'stop_transfer', 'off') != 'on'"
    already_paid.joins(booking: :reservations).where(where_sql)
  end)
  scope :scheduled_to_pay_guides, (lambda do
    where_sql = "(payment_transactions.other_details->'transfers') IS NOT NULL"
    scheduled_transfer_payments.where(where_sql)
  end)
  scope :payouts, (lambda do |user_id|
    sql = "(payment_transactions.other_details->'payment_to_guides') IS NOT NULL
    AND (reservations.host_id = :uid OR reservations.pair_guide_id = :uid)"
    joins(booking: :reservations).where(sql, uid: user_id)
  end)

  def total_amount(currency = DEFAULT_CURRENCY, stripe_charge: true)
    return amount unless stripe_charge
    stripe_amount(amount, currency)
  end

  def make_charge
    return false unless proceed_to_stripe_charge?
    make_test_card!(user) unless stripe_card_testing?(user)
    charges = first_or_create_charges(self)
   
    save_transaction_info!(charges)
    save_transaction_details!(charges)
    #puts "aaaaaaaaaaaaaaaaaaaaaa #{self.inspect}"
    #puts "sssssssssssssss #{charges }"
  rescue => e
    errors.add(:base, I18n.t('.transaction.error_message', error: e.message))
    log_debug(e, { 'from make_charge method': charges.inspect })
  end

  def save_transaction_details!(charges = nil)
    
    unless charges
      # DELAYED PAYMENT TRANSACTION
      other_details[:booking_date] = Time.zone.now.strftime('%Y/%m/%d %H:%M')
      return save
    end
    # ON LAST TOUR DATE
    save_charge_details!(charges)
  end

  def save_transaction_info!(charges)
    charges = [charges] unless charges.is_a?(Array)
    main_charge = charges.select { |chg| chg.metadata[:user_type] == 'main' }[0]
    
    reservation_ids = reservations.keys
    charges.each do |charge|
      metadata = charge.metadata.clone
      add_condition = metadata[:reservation_ids] == reservation_ids.join(',')
      is_main_guide = metadata[:user_type] == 'main'
      unless is_main_guide
        add_condition = metadata[:reservation_id].in?(reservation_ids)
      end
      add_condition = metadata[:charge_only] == 'yes' unless main_charge

      if metadata[:transaction_id].to_i == id &&
         metadata[:booking_id].to_i == booking_id &&
         metadata[:user_id].to_i == user_id &&
         add_condition
        next
      end
      if is_main_guide || main_charge.blank?
        charge.metadata[:reservation_ids] = reservation_ids.join(',')
        charge.metadata[:reservation_id] = nil
      else
        charge.metadata[:parent_charge_id] = main_charge.id
      end
      charge.metadata[:transaction_id] = id
      charge.metadata[:booking_id] = booking_id
      charge.metadata[:user_id] = user_id
      charge.save
    end
  end

  def save_charge_details!(charges, set_paid_date: true)
    
    charges = [charges] unless charges.is_a?(Array)
 
    other_details['charges'] ||= {}
    main_charge = charges.select { |chg| chg.metadata[:user_type] == 'main' }[0]
    charges.each do |charge|
      user_type = charge.metadata[:user_type]
      reservation_id = charge.metadata[:reservation_id]
      key_suffix = user_type == 'main' ? '' : "_#{reservation_id}"
      key = "#{user_type}_guide#{key_suffix}"
      charge_details = payment_charges["#{key}"]
      next if charge_details.present? && charge_details.to_h == charge.to_h
      other_details['charges']["#{key}"] = charge.to_h
    end
    now = Time.zone.now.strftime('%Y/%m/%d %H:%M')
    other_details[:booking_date] = now if other_details['booking_date'].blank?
    if main_charge.blank? && charges.length == 1
      main_charge = charges.first
      main_charge = nil if main_charge.metadata[:charge_only] != 'yes'
    end
    return save unless main_charge
    other_details['paid_date'] = now if set_paid_date
    update(paid: main_charge.paid?, stripe_charge_id: main_charge.id)
  end

  def do_automatic_stripe_charge?
    proceed_to_stripe_charge? && on_or_after_last_tour_date? && to_charge_guest_reservations?
  end

  def proceed_to_stripe_charge?
    booking_offered? && not_yet_transacted? && guest_registered_in_stripe?
  end

  def unpaid?
    stripe_charge_id.blank? && paid.blank? && other_details['paid_date'].blank?
  end

  def paid_already?
    payment_charges.length > 0 && !unpaid?
  end

  def not_yet_transacted?
    unless (not_yet_transacted = !paid_already?)
      errors.add(:base, I18n.t('.transaction.already_paid'))
    end
    not_yet_transacted
  end

  def guest_registered_in_stripe?
    unless (registered_in_stripe = user.can_make_stripe_payments?)
      errors.add(
        :base,
        I18n.t('.transaction.stripe_unregistered_guest', booking_id: booking_id)
      )
    end
    registered_in_stripe
  end

  def booking_offered?
    unless (offered = booking.finalized?)
      err_code = 'booking_not_offered'
      err_msg = I18n.t(".transaction.#{err_code}", booking_id: booking_id)
      errors.add(:base, err_msg)
    end
    offered
  end

  def on_or_after_last_tour_date?
    last_tour = booking.reservations.charged_the_guest.last_scheduled_tour
    last_tour_date = last_tour.try(:schedule).try(:to_date)
    return false unless last_tour_date
    Time.zone.now.to_date >= last_tour_date
  end

  def guides_can_receive_stripe_payments?(reserv_id = nil)
    stripe_registered_guides = true
    guides_reservs = guides_reservations(reserv_id)
    guides_reservs.each do |guide|
      stripe_registered_guides &&= guide.can_receive_stripe_payments?
    end
    guides_reservs.exists? && stripe_registered_guides
  end

  def compute_charged_fee
    (main_guide_price + support_guide_price) * PERCENT_CHARGE_FEE
  end

  def compute_amount
    # NOTE: This computation is only based from `amount` method in reservation
    # model (see for reference). This has been refactored here for payment
    # transaction clarity as moving forward towards Stripe integration
    (
      basic_price + charged_fee.to_f + currency_conversion_price
    ) - (
      coupons_price + campaign_price
    )
  end

  def compute_reservations_amount
    reservations.sum { |_key, reserv| reserv.total_amount } - coupons_price
  end

  def basic_price
    main_guide_price + support_guide_price + others_price + insurance_price
  end

  def main_guide_price(reservation_id = nil)
    reserv8ns = booking.reservations
    reserv8ns = reserv8ns.where(id: reservation_id.to_i) if reservation_id
    reserv8ns.map do |reservation|
      reservation.price + reservation.transportation_cost_main
    end.sum
  end

  def support_guide_price(reservation_id = nil)
    reserv8ns = booking.reservations
    reserv8ns = reserv8ns.where(id: reservation_id.to_i) if reservation_id
    reserv8ns.map do |reservation|
      reservation.price_for_support + reservation.transportation_cost_support
    end.sum
  end

  def insurance_price
    booking.reservations.map(&:insurance_fee).sum
  end

  def others_price(reservation_id = nil)
    reserv8ns = booking.reservations
    reserv8ns = reserv8ns.where(id: reservation_id.to_i) if reservation_id
    reserv8ns.map(&:option_amount).sum
  end

  def currency_conversion_price(fee = nil)
    return 0 if default_currency?
    ((fee || (basic_price + charged_fee.to_f)) * PERCENT_CONVERSION_FEE).ceil
  end

  def coupons_price
    return 0 unless coupons?
    Coupon.where(id: other_details['coupon_ids']).sum(:amount).to_i
  end

  def campaign_price
    # NOTE: as per clarification from Japan team, FOR NOW, service charge fee
    # and campaign discount are the same which means service charge is for free
    charged_fee.to_f
  end

  def main_guide_payment(reservation_id = nil)
    reservs_count = reservation_id ? 1 : reservations.length
    main_guide_price(reservation_id) - (GUIDE_CHARGE_FEE * reservs_count)
  end

  def support_guide_payment(reservation_id = nil)
    reservs_count = reservation_id ? 1 : reservations.length
    support_guide_price(reservation_id) - (GUIDE_CHARGE_FEE * reservs_count)
  end

  def done?
    paid? || other_details['booking_date'].present?
  end

  def reservations
    reservation_details = {}
    (other_details['reservations'] || {}).each do |key, reservation|
      reservation_details["#{key}"] = OpenStruct.new(
        reservation.to_h.with_indifferent_access
      )
    end
    reservation_details
  end

  def payment_charges
    charges = {}
    (other_details['charges'] || {}).each do |key, charge|
      charges["#{key}"] = OpenStruct.new(charge.to_h.with_indifferent_access)
    end
    charges
  end

  def guides_reservations(reserv_id = nil)
    if reserv_id.present?
      reserv = booking.reservations.find_by(id: reserv_id.to_i)
      reserv.present? ? reserv.users.where.not(id: user_id) : []
    else
      ([[booking.guide, nil]] +
        booking.reservations.charged_the_guest.map do |reservation|
          next unless reservation.pg_completion?
          [reservation.pair_guide, reservation.id]
        end).compact
    end
  end

  def guide_payments
    payments = {}
    (other_details['payment_to_guides'] || {}).each do |key, payout|
      if key['_guide'].present?
        # old Stripe implementation 'Destination Charges'
        payments["#{key}"] = OpenStruct.new(payout.to_h.with_indifferent_access)
      else
        # new Stripe implementation 'Separate Charges and Transfers'
        payments["#{key}"] ||= {}
        payout.each do |guide_id, stripe_payout|
          payments["#{key}"]["#{guide_id}"] = OpenStruct.new(
            stripe_payout.to_h.with_indifferent_access
          )
        end
      end
    end
    payments
  end

  def refunds
    reservation_refunds = {}
    (refund_details['reservations'] || {}).each do |reserv_id, refund|
      reservation_refunds["#{reserv_id}"] = OpenStruct.new(
        refund.to_h.with_indifferent_access
      )
    end
    reservation_refunds
  end

  def transfer_payments
    transfers = {}
    (other_details['transfers'] || {}).each do |reserv_id, my_transfers|
      transfers["#{reserv_id}"] ||= {}
      my_transfers.each do |guide_id, my_transfer|
        transfers["#{reserv_id}"]["#{guide_id}"] = OpenStruct.new(
          my_transfer.to_h.with_indifferent_access
        )
      end
    end
    transfers
  end

  def make_refund(reservation_id, opts)
    return unless refund_valid?(reservation_id, opts)
    refund_by_charges_only!(reservation_id, opts)
  rescue => e
    errors.add(:base, e.message)
    log_debug(e, {
      'from make_refund method': "refund for reservation id: #{reservation_id}"
    })
  end

  def refund_by_charges_only!(reservation_id, opts)
    refund = nil
    charge = payment_charges.values.try(:first)
    details = prepare_refund_details(reservation_id, opts)
    if charge && charge.metadata['charge_only'] == 'yes'
      details[:charge_only] = 'yes'
      if within_90_days_from_paid_date? &&
         qualified_for_refund?(reservation_id, opts)
        charge_id = charge.try(:id) || charge[:id] || stripe_charge_id
        # check first if there's still balance
        current_balance = charge.amount - compute_refund(nil, 0)
        refund_amount = details[:amount] - details[:refund_fee]
        if refund_amount > current_balance && current_balance > 0
          details[:amount] = current_balance
          details[:refund_fee] = 0
        end
        #/ check first if there's still balance
        refund = first_or_create_refund(self, details, charge_id)
      end
      save_refund_info!(refund, reservation_id)
    else
      # support previous tour reservations that are not yet finished
      return refund_by_charges!(reservation_id, opts)
    end
    save_refund_details!(refund, details)
  end

  def refund_by_charges!(reservation_id, opts)
    charges = payment_charges.to_a.reverse.to_h
    success = true
    charges.each do |key, charge|
      support_guide = key['guide_'].present?
      opts[:amount] = charge.amount
      details = prepare_refund_details(reservation_id, opts)
      next if support_guide && key["guide_#{reservation_id}"].blank?
      details[:guide_type] = support_guide ? key.rpartition('_').first : key
      refund = nil
      if qualified_for_refund?(reservation_id, opts)
        charge_id = charge.try(:id) || charge[:id] || stripe_charge_id
        refund = first_or_create_refund(self, details, charge_id)
      end
      save_refund_info!(refund, reservation_id)
      success &&= save_refund_details!(refund, details)
    end
    return success if charges.length > 0
    save_refund_details!(nil, prepare_refund_details(reservation_id, opts))
  end

  def save_refund_details!(refund, details)
    if refund
      details[:refund_id] = refund.id
      details[:refund_data] = refund.to_h.except(:id)
    end
    reservation_id = details[:reservation_id]
    refund_details['reservations'] ||= {}
    refund_details['reservations']["#{reservation_id}"] ||= {}
    refunded = refund_details['reservations']["#{reservation_id}"]
    if (guide_type = details[:guide_type])
      refunded["#{guide_type}"] = details.except(
        :refunded_at, :refunded_by, :refund_rate
      )
    end
    refunded['refunded_at'] = details[:refunded_at]
    refunded['refunded_by'] = details[:refunded_by]
    refunded['refund_rate'] = details[:refund_rate]
    refunded['details'] = details.except(
      :refunded_at, :refunded_by, :refund_rate
    )
    save
  end

  def save_refund_info!(refund, reservation_id)
    return unless refund
    refund.metadata[:transaction_id] = id
    refund.metadata[:booking_id] = booking_id
    refund.metadata[:reservation_id] = reservation_id
    refund.save
  end

  def refund_fee(reservation_id, opts = {})
    (opts[:amount] || reservations["#{reservation_id}"].total_amount) *
      refund_percentage(reservation_id, opts)
  end

  def refund_percentage(reservation_id, opts = {})
    refunded_by = opts[:refunded_by]
    refunded_at = opts[:refunded_at] || Time.zone.now
    refunded_at = Time.zone.parse(refunded_at) if refunded_at.is_a?(String)
    schedule = booking.reservations.find(reservation_id.to_i).schedule
    weeks_ago = (schedule - 14.days).at_end_of_day.change(sec: 0)
    days_ago = (schedule - 3.days).at_end_of_day.change(sec: 0)
    if refunded_by == 'guide' || refunded_at <= weeks_ago
      0
    elsif refunded_at > weeks_ago && refunded_at <= days_ago
      0.5
    elsif refunded_at > days_ago
      1
    end
  end

  def compute_refund(reservation_id, amount, opts = {})
    details = refunds["#{reservation_id}"]
    guide_type = opts[:guide_type]
    if guide_type && details && reservation_id &&
       guide_type.try(:[], /(main|support)+/) &&
       qualified_for_refund?(reservation_id, details.to_h)
      compute_rate_deduction(amount, details.refund_rate)
    elsif guide_type == 'main' && details.blank? &&
          reservation_id.blank? && refunds.present?
      refunds.map do |reserv_id, ref|
        next unless qualified_for_refund?(reserv_id, ref.to_h)
        if opts[:purpose] == 'guide_payment'
          compute_rate_deduction(main_guide_price(reserv_id), ref.refund_rate)
        else
          ref.details['amount'].to_f - ref.details['refund_fee'].to_f
        end
      end.compact.sum
    elsif opts.blank? && reservation_id.blank? && refunds.present?
      refunds.map do |reserv_id, ref|
        next unless qualified_for_refund?(reserv_id, ref.to_h)
        reservation = reservations["#{reserv_id}"]
        compute_rate_deduction(reservation.total_amount, ref.refund_rate)
      end.compact.sum
    else
      0
    end
  end

  def qualified_for_refund?(reservation_id, opts = {})
    refunded_by = opts[:refunded_by]
    refunded_at = opts[:refunded_at] || Time.zone.now
    refunded_at = Time.zone.parse(refunded_at) if refunded_at.is_a?(String)
    schedule = booking.reservations.find(reservation_id.to_i).schedule
    days_ago = (schedule - 3.days).at_end_of_day.change(sec: 0)
    refunded_by == 'guide' || refunded_at <= days_ago
  end

  def no_selected_support_guide_fees
    # if there's a support guide, his/her payment will be deducted
    # otherwise, no deduction in order to include it as payment to Huber
    booking.reservations.map do |reservation|
      # since we use this in support_price_excluding_refunded method which is
      # used to deduct from the payment total amount
      # (see compose_details method in chargeable.rb)
      # the implementation of above rules about support guide's payment will be
      # negation (or opposite because the usage is in substraction not addition)
      unless reservation.pg_completion?
        reservation.price_for_support + reservation.transportation_cost_support
      end
    end.compact.sum
  end

  def support_price_excluding_refunded
    support_guide_price -
      refunds.map do |key, ref|
        compute_rate_deduction(support_guide_price(key), ref.refund_rate)
      end.sum - no_selected_support_guide_fees
  end

  def compute_guide_charge_fee(reservation_id)
    GUIDE_CHARGE_FEE *
      if (ref = refunds["#{reservation_id}"]) || reservation_id
        # for support guide
        1 - ((100 - (ref.try(:refund_rate) || 100)).to_f / 100)
      else
        # for main guide
        reservations.length -
          refunds.map { |_id, ref| ((100 - ref.refund_rate).to_f / 100) }.sum
      end
  end

  def store_encountered_error!(error, reset_details: true)
    reset_details_when_error! if reset_details
    timestamp = Time.zone.now.to_i
    other_details['errors'] ||= {}
    other_details['errors'][timestamp] ||= {}
    error_hash = other_details['errors'][timestamp]
    error_hash[:message] = error.inspect
    error_hash[:backtrace] = error.backtrace.join("\n")
    save
  end

  def invalid_tour_details?
    booking.reservations.each do |reservation|
      return true if reservation.time_required <= 0
    end
    false
  end

  def save_coupons(coupon_ids = nil)
    other_details['coupon_ids'] = coupon_ids
    set_amount
    save
  end

  def coupons?
    other_details['coupon_ids'].present?
  end

  def log_debug(exception, opts = {})
    back_trace = exception.backtrace.join("\n")
    log_opts = opts.blank? ? '' : " - #{opts.inspect}"
    log_msg = "#{Time.zone.now}#{log_opts}: #{exception.inspect}\n#{back_trace}"
    Rails.logger.send("#{ opts[:log_info] ? 'info' : 'debug' }", log_msg)
    return if opts[:no_raise].present?
    raise ActiveRecord::Rollback, log_msg, exception.backtrace
  end

  def to_charge_guest_reservations?(reserv_id = nil)
    to_charge_guest = true
    reservs = booking.reservations
    reservs = reservs.where(id: reserv_id.to_i) if reserv_id.present?
    reservs.each do |reservation|
      to_charge_guest &&= reservation.to_charge_guest?
    end
    reservs.exists? && to_charge_guest
  end

  def refundable_to_guest(reservation_id)
    return 0 unless (refund_info = refunds["#{reservation_id}"])
    refund_info.details['amount'] - refund_info.details['refund_fee']
  end

  def default_currency?
    other_details['currency'].try(:upcase) == DEFAULT_CURRENCY.upcase
  end

  def within_90_days_from_paid_date?
    paid_date = other_details['paid_date'].try(:to_date)
    paid_date.present? && Time.zone.now.to_date <= (paid_date + 90.days)
  end

  private

  def set_charged_fee
    return unless booking.finalized?
    return if charged_fee == compute_charged_fee
    update_attribute :charged_fee, compute_charged_fee
  end

  def set_amount
    return unless booking.finalized?
    new_amount = compute_amount
    reservs_amount = compute_reservations_amount
    new_amount = reservs_amount if new_amount.to_f != reservs_amount.to_f
    return if amount == new_amount
    update_attribute :amount, new_amount
  end

  def set_currency
    return unless booking.finalized?
    currency = currency_by_country(user)
    return if currency == other_details['currency']
    other_details['currency'] = currency
    save
  end

  def set_reservation_details
    return unless booking.finalized?
    reservation_info = {}
    booking.reservations.each do |reserv|
      next unless reserv.schedule
      reservation_info["#{reserv.id}"] = reservation_hash(reserv)
    end
    old_reservations = (other_details['reservations'] || {}).map do |key, val|
      [key, val.sort.to_h]
    end.to_h
    return if reservation_info.blank? || reservation_info == old_reservations
    other_details['reservations'] = reservation_info
    save
  end

  def reservation_hash(reservation)
    sched = reservation.schedule
    guide_fee = reservation.price + reservation.transportation_cost_main +
                reservation.price_for_support +
                reservation.transportation_cost_support
    others_fee = reservation.option_amount
    other_expenses = reservation.reservation_extra_costs.map do |extra|
      people = reservation.num_of_people + 2
      additional_text = ''
      additional_text = "(#{reservation.people_str(people)})" if extra.person?
      expense = extra.price * people
      expense = extra.price if extra.team?
      { 'fee' => expense, 'label' => "#{extra.description} #{additional_text}" }
    end
    service_fee = guide_fee * PERCENT_CHARGE_FEE
    campaign_fee = service_fee
    insurance_fee = reservation.insurance_fee
    reserv8n_amount = guide_fee + others_fee + service_fee + insurance_fee
    conversion_fee = currency_conversion_price(reserv8n_amount)
    total_amount = (reserv8n_amount + conversion_fee) - campaign_fee
    {
      'guide_fee' => guide_fee, 'others_fee' => others_fee,
      'service_commission' => service_fee, 'campaign_discount' => campaign_fee,
      'other_expenses' => other_expenses, 'travel_insurance' => {
        'fee' => insurance_fee, 'label' => "(#{reservation.people_str})"
      },
      'exchange_fee' => conversion_fee,
      'tour_date' => sched.strftime('%m/%d/%Y'),
      'duration' => reservation.time_required.to_s,
      'meeting_at' => reservation.place,
      'meeting_time' => sched.strftime('%Y/%m/%d %H:%M'),
      'number_of_people' => reservation.people_str,
      'specific_meeting_at' => reservation.place_memo, 'id' => reservation.id,
      'guests_cost' => reservation.guests_cost,
      'note' => reservation.description, 'total_amount' => total_amount
    }.sort.to_h
  end

  def set_details
    return unless booking.finalized?
    self.other_details = other_details.try(:with_indifferent_access) || {}
    self.refund_details = refund_details.try(:with_indifferent_access) || {}
  end

  def prepare_refund_details(reservation_id, opts = {})
    refunded_by = opts[:refunded_by]
    charge_amount = opts[:amount] ||
                    reservations["#{reservation_id}"].total_amount
    {
      refunded_at: Time.zone.now.strftime('%Y/%m/%d %H:%M:%S'),
      refund_fee: refund_fee(reservation_id, opts), refunded_by: refunded_by,
      refund_rate: 100 * refund_percentage(reservation_id, opts),
      amount: charge_amount, reason: opts[:reason], # reverse_transfer: true,
      # temporary disable the reverse_transfer as part of disabling automatic
      # payment to guides [charge only, no transfer to destination account]
      transaction_id: id, booking_id: booking_id, reservation_id: reservation_id
    }
  end

  def refund_valid?(reservation_id, opts)
    return if no_refund_reason?(opts)
    return if !opts[:mark_as_refund] && refunded?(reservation_id)
    true
  end

  def no_refund_reason?(opts)
    return if opts[:user_type] == 'guest' || opts[:reason].present?
    errors.add(:base, I18n.t('.transaction.no_refund_reason'))
  end

  def refunded?(reservation_id)
    details = refunds["#{reservation_id}"]
    return if details.blank?
    reservation = booking.reservations.find(reservation_id.to_i)
    return if details.try(:refund_id).blank? &&
              !reservation.canceled_after_accepted?
    errors.add(
      :base,
      I18n.t(
        '.transaction.already_refunded',
        at: details.refunded_at, by: details.refunded_by
      )
    )
  end

  def compute_rate_deduction(fee, rate)
    fee - (fee * (rate.to_f / 100))
  end

  def reset_details_when_error!
    info = other_details
    info.delete('charges') if info.key?('charges')
    info.delete('transfers') if info.key?('transfers')
    info.delete('payment_to_guides') if info.key?('payment_to_guides')
    info.delete('paid_date') if info.key?('paid_date')
    update(paid: false, stripe_charge_id: nil) if paid? || stripe_charge_id?
  end

  def self.search_stop_transfer(status)
    where("other_details -> 'stop_transfer' ? :status", status: status)
  end
end
