require 'rails_helper'

RSpec.describe PaymentTransaction, type: :model do
  let(:booking) { Booking.create }
  let(:user) { User.create }
  let(:payment_transaction) { PaymentTransaction.new(amount: 100.00, user_id: user.id, booking_id: booking.id ) }
  let(:stripe_helper) { StripeMock.create_test_helper }
  before { StripeMock.start }
  after { StripeMock.stop }
  let(:currency){ PaymentTransaction::DEFAULT_CURRENCY }

  describe '#total_amount' do
    it 'returns the amount if no stripe charge' do
      expect( payment_transaction.total_amount(currency, stripe_charge: false) ).to eq 100.00
    end

    it 'returns the stripe_amount if there is stripe charge' do
      expect( payment_transaction.total_amount(currency, stripe_charge: true) ).to eq 105.00
    end
  end

  describe '#make_charge' do
    let(:customer) { Stripe::Customer.create({ email: 'johnny@appleseed.com', source: stripe_helper.generate_card_token}) }
    let(:main_charge) { Stripe::Charge.create({ amount: 100, currency: currency, description: 'Example charge', customer: customer, metadata: { user_type: 'main' } }) }
    let(:sub_charge) { Stripe::Charge.create({ amount: 50, currency: currency, description: 'Example charge1', customer: customer, metadata: { user_type: 'sub' } }) }
    
    it 'returns false if proceed_to_stripe_charge? is false' do
      allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return(false)
 
      expect( payment_transaction.make_charge ).to eq false
    end

    context 'when there is main charge object' do
      it 'sets charge and payment details' do
        allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return(true)
        allow_any_instance_of(StripeTransaction).to receive(:stripe_card_testing?).with(payment_transaction.user).and_return( true )
        allow(payment_transaction).to receive(:reservations).and_return({"111" => "value", "222" => "value"})
   
        allow_any_instance_of(StripeTransaction).to receive(:first_or_create_charges).with(payment_transaction).and_return( [main_charge, sub_charge] )
  
        payment_transaction.make_charge
        expect( payment_transaction.paid ).to eq true
        expect( payment_transaction.stripe_charge_id ).to eq "test_ch_4"
        expect( payment_transaction.other_details["booking_date"] ).to eq Time.zone.now.strftime('%Y/%m/%d %H:%M')
      end

      it 'sets metadata for charge objects' do
        allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return(true)
        allow_any_instance_of(StripeTransaction).to receive(:stripe_card_testing?).with(payment_transaction.user).and_return( true )
        allow(payment_transaction).to receive(:reservations).and_return({"111" => "value", "222" => "value"})
  
        allow_any_instance_of(StripeTransaction).to receive(:first_or_create_charges).with(payment_transaction).and_return( [main_charge, sub_charge] )
  
        payment_transaction.make_charge
        expect( main_charge.metadata["reservation_ids"] ).to eq "111,222"
        expect( main_charge.metadata["booking_id"] ).to eq booking.id
        expect( main_charge.metadata["user_id"] ).to eq user.id
        expect( main_charge.metadata["reservation_id"] ).to eq ""
    
        expect( sub_charge.metadata["parent_charge_id"] ).to eq main_charge.id
        #expect( sub_charge.metadata["charge_only"] ).to eq "yes"
      end

      context 'when there is no main charge object' do
        it 'sets metadata for charge objects' do
          allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return(true)
          allow_any_instance_of(StripeTransaction).to receive(:stripe_card_testing?).with(payment_transaction.user).and_return( true )
          allow(payment_transaction).to receive(:reservations).and_return({"111" => "value", "222" => "value"})
  
          allow_any_instance_of(StripeTransaction).to receive(:first_or_create_charges).with(payment_transaction).and_return( [sub_charge] )
  
          payment_transaction.make_charge
        
          expect( sub_charge.metadata["charge_only"] ).to eq "yes"
        end
      end
    end
  end

  describe '#unpaid?' do
    it 'returns true if stripe_charge_id is blank' do
      expect( payment_transaction.unpaid? ).to eq true
    end

    it 'returns true if paid is blank' do
      expect( payment_transaction.unpaid? ).to eq true
    end

    it 'returns true if other_details["paid_date"] is blank' do
      expect( payment_transaction.unpaid? ).to eq true
    end

    it 'returns false if stripe_charge_id, paid and other_details["paid_date"] are not blank' do
      payment_transaction.stripe_charge_id = 1
      payment_transaction.paid = true
      payment_transaction.other_details = {"paid_date" => Date.today}

      expect( payment_transaction.unpaid? ).to eq false
    end

  end

  describe '#paid_already?' do
    it 'returns false if unpaid is true' do
      allow(payment_transaction).to receive(:unpaid?).and_return(true)
 
      expect( payment_transaction.paid_already? ).to eq false
    end
    
    it 'returns false 0 payment_charges' do
      allow(payment_transaction).to receive(:payment_charges).and_return({})
 
      expect( payment_transaction.paid_already? ).to eq false
    end
 
    it 'returns true if more than one payment_charges and unpaid is false' do
      allow(payment_transaction).to receive(:unpaid?).and_return( false )
      allow(payment_transaction).to receive(:payment_charges).and_return( {"test" => OpenStruct.new("1" => 10.0)} )
  
      expect( payment_transaction.paid_already? ).to eq true
    end
  end

  describe '#proceed_to_stripe_charge?' do
    it 'returns false if booking_offered? is false' do
      allow(payment_transaction).to receive(:booking_offered?).and_return( false )
 
      expect( payment_transaction.proceed_to_stripe_charge? ).to eq false
    end
    
    it 'returns false if not_yet_transacted is false' do
      allow(payment_transaction).to receive(:not_yet_transacted?).and_return( false )
 
      expect( payment_transaction.proceed_to_stripe_charge? ).to eq false
    end

    it 'returns false if guest_registered_in_stripe is false' do
      allow(payment_transaction).to receive(:guest_registered_in_stripe?).and_return( false )
  
      expect( payment_transaction.proceed_to_stripe_charge? ).to eq false
    end
 
    it 'returns true if booking_offered, guest_registered_in_stripe and not_yet_transacted are true' do
      allow(payment_transaction).to receive(:booking_offered?).and_return( true )
      allow(payment_transaction).to receive(:not_yet_transacted?).and_return( true )
      allow(payment_transaction).to receive(:guest_registered_in_stripe?).and_return( true )
 
      expect( payment_transaction.proceed_to_stripe_charge? ).to eq true
    end
  end

  describe '#do_automatic_stripe_charge?' do
 
    it 'returns false if proceed_to_stripe_charge? is false' do
      allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return( false )
 
      expect( payment_transaction.do_automatic_stripe_charge?).to eq false
    end
    
    it 'returns false if on_or_after_last_tour_date is false' do
      allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return( false )
      allow(payment_transaction).to receive(:on_or_after_last_tour_date?).and_return( false )
    
      expect( payment_transaction.do_automatic_stripe_charge? ).to eq false
    end

    it 'returns false if to_charge_guest_reservations is false' do
      allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return( false )
      allow(payment_transaction).to receive(:on_or_after_last_tour_date?).and_return( false )
      allow(payment_transaction).to receive(:to_charge_guest_reservations?).and_return( false )

      expect( payment_transaction.do_automatic_stripe_charge? ).to eq false
    end
 
    it 'returns true if booking_offered, guest_registered_in_stripe and not_yet_transacted are true' do
      allow(payment_transaction).to receive(:proceed_to_stripe_charge?).and_return( true )
      allow(payment_transaction).to receive(:on_or_after_last_tour_date?).and_return( true )
      allow(payment_transaction).to receive(:to_charge_guest_reservations?).and_return( true )
 
      expect( payment_transaction.do_automatic_stripe_charge? ).to eq true
    end
  end
end
