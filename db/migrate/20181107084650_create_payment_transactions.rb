class CreatePaymentTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_transactions do |t|
      t.integer :user_id          
      t.integer :booking_id 
      t.decimal :amount, precision: 8, scale: 2, default: 0.0, null: false
      t.boolean :paid, default: false
      t.decimal :charged_fee, precision: 8, scale: 2, default: 0.0 
      t.string  :stripe_charge_id
      t.jsonb   :refund_details
      t.jsonb   :other_details
 
      t.timestamps
    end
  end
end
